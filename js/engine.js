$(function() {



$(document).ready(function() {
    var title = {
       text: 'График инвестиции'   
    };
    var subtitle = {
       text: ''
    };
    var xAxis = {
       categories: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
          'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    };
    var yAxis = {
       title: {
          text: 'Цена (\xB0руб)'
       },
       plotLines: [{
          value: 0,
          width: 1,
          color: '#808080'
       }]
    };   

    var tooltip = {
       valueSuffix: '\xB0руб'
    }
    var legend = {
       // layout: 'vertical',
       // align: 'right',
       // verticalAlign: 'middle',
       // borderWidth: 0
    };
    var series =  [{
          name: 'BMW GT 630 I',
          data: [758234, 778348, 600396, 700490, 770890, 790100, 829000,
             888900, 856000, 890006, 900400, 930987]
       }, 

    ];

    var json = {};
    json.title = title;
    json.subtitle = subtitle;
    json.xAxis = xAxis;
    json.yAxis = yAxis;
    json.tooltip = tooltip;
    json.legend = legend;
    json.series = series;

    $('#chart-1').highcharts(json);
    $('#chart-2').highcharts(json);
    $('#chart-3').highcharts(json);
 });










// Тут не достаточно знаний в программировании, что бы нормально сделать :( #sliderAutomobile-1, #sliderAutomobile-2
$('#sliderAutomobile-1').owlCarousel({
        loop: false,
        margin: 10,
        responsiveClass: true,
        nav: true,
        items: 1,
        slideBy: 1,
        //lazyLoad:true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            }
        },
        onInitialized: function () {
            var currSlide = $('#sliderAutomobile-1 .owl-dots .owl-dot.active').index() + 1,

                length = $('#sliderAutomobile-1 .owl-dots .owl-dot').length;

            if (currSlide < 10) {
                currSlide = '0' + currSlide;
            }
            if (length < 10) {
                length = '0' + length;
            }
            $('#sliderAutomobile-1 .owl-dots').wrap('<div class="dots_wrap">');
            $('#sliderAutomobile-1 .dots_wrap').prepend('<div class="numSlidesDotscurr">' + currSlide + '</div>');
            $('#sliderAutomobile-1 .dots_wrap').append('<div class="numSlidesDotsAll">' + length + '</div>');

        },
        onTranslate: function () {
            var currSlide = $('#sliderAutomobile-1 .owl-dots .owl-dot.active').index() + 1,
                length = $('#sliderAutomobile-1 .owl-dots .owl-dot').length;
            if (currSlide < 10) {
                currSlide = '0' + currSlide;
            }
            if (length < 10) {
                length = '0' + length;
            }
            $('#sliderAutomobile-1 .numSlidesDotscurr').html(currSlide);
            $('#sliderAutomobile-1 .numSlidesDotsAll').html(length);
        },
        onResize: function () {
            var currSlide = $('#sliderAutomobile-1 .owl-dots .owl-dot.active').index() + 1,
                length = $('#sliderAutomobile-1 .owl-dots .owl-dot').length;
            if (currSlide < 10) {
                currSlide = '0' + currSlide;
            }
            if (length < 10) {
                length = '0' + length;
            }
            $('#sliderAutomobile-1 .numSlidesDotscurr').html(currSlide);
            $('#sliderAutomobile-1 .numSlidesDotsAll').html(length);
        }
    });


$('#sliderAutomobile-2').owlCarousel({
        loop: false,
        margin: 10,
        responsiveClass: true,
        nav: true,
        items: 1,
        slideBy: 1,
        //lazyLoad:true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            }
        },
        onInitialized: function () {
            var currSlide = $('#sliderAutomobile-2 .owl-dots .owl-dot.active').index() + 1,
                length = $('#sliderAutomobile-2 .owl-dots .owl-dot').length;

            if (currSlide < 10) {
                currSlide = '0' + currSlide;
            }
            if (length < 10) {
                length = '0' + length;
            }
            $('#sliderAutomobile-2 .owl-dots').wrap('<div class="dots_wrap">');
            $('#sliderAutomobile-2 .dots_wrap').prepend('<div class="numSlidesDotscurr">' + currSlide + '</div>');
            $('#sliderAutomobile-2 .dots_wrap').append('<div class="numSlidesDotsAll">' + length + '</div>');

        },
        onTranslate: function () {
            var currSlide = $('#sliderAutomobile-2 .owl-dots .owl-dot.active').index() + 1,
                length = $('#sliderAutomobile-2 .owl-dots .owl-dot').length;
            if (currSlide < 10) {
                currSlide = '0' + currSlide;
            }
            if (length < 10) {
                length = '0' + length;
            }
            $('#sliderAutomobile-2 .numSlidesDotscurr').html(currSlide);
            $('#sliderAutomobile-2 .numSlidesDotsAll').html(length);
        },
        onResize: function () {
            var currSlide = $('#sliderAutomobile-2 .owl-dots .owl-dot.active').index() + 1,
                length = $('#sliderAutomobile-2 .owl-dots .owl-dot').length;
            if (currSlide < 10) {
                currSlide = '0' + currSlide;
            }
            if (length < 10) {
                length = '0' + length;
            }
            $('#sliderAutomobile-2 .numSlidesDotscurr').html(currSlide);
            $('#sliderAutomobile-2 .numSlidesDotsAll').html(length);
        }
    });







// Прикрепляем файлы
$('.attach').each(function() { // на случай, если таких групп файлов будет больше одной
  var attach = $(this),
    fieldClass = 'attach__item', // класс поля
    attachedClass = 'attach__item--attached', // класс поля с файлом
    fields = attach.find('.' + fieldClass).length, // начальное кол-во полей
    fieldsAttached = 0; // начальное кол-во полей с файлами

  var newItem = '<div class="attach__item"><label><div class="attach__up">Добавить файл</div><input class="attach__input" type="file" name="files[]" /></label><div class="attach__delete">&#10006;</div><div class="attach__name"></div><div class="attach__edit">Изменить</div></div>'; // разметка нового поля

  // При изменении инпута
  attach.on('change', '.attach__input', function(e) {
    var item = $(this).closest('.' + fieldClass),
      fileName = '';
    if (e.target.value) { // если value инпута не пустое
      fileName = e.target.value.split('\\').pop(); // оставляем только имя файла и записываем в переменную
    }
    if (fileName) { // если имя файла не пустое
      item.find('.attach__name').text(fileName); // подставляем в поле имя файла
      if (!item.hasClass(attachedClass)) { // если в поле до этого не было файла
        item.addClass(attachedClass); // отмечаем поле классом
        fieldsAttached++;
      }
      if (fields < 10 && fields == fieldsAttached) { // если полей меньше 10 и кол-во полей равно
        item.after($(newItem)); // добавляем новое поле
        fields++;
      }
    } else { // если имя файла пустое
      if (fields == fieldsAttached + 1) {
        item.remove(); // удаляем поле
        fields--;
      } else {
        item.replaceWith($(newItem)); // заменяем поле на "чистое"
      }
      fieldsAttached--;

      if (fields == 1) { // если поле осталось одно
        attach.find('.attach__up').text('ЗАГРУЗИТЬ СКАН ДОГОВОРА'); // меняем текст
      }
    }
  });

  // При нажатии на "Изменить"
  attach.on('click', '.attach__edit', function() {
    $(this).closest('.attach__item').find('.attach__input').trigger('click'); // имитируем клик на инпут
  });

  // При нажатии на "Удалить"
  attach.on('click', '.attach__delete', function() {
    var item = $(this).closest('.' + fieldClass);
    if (fields > fieldsAttached) { // если полей больше, чем загруженных файлов
      item.remove(); // удаляем поле
      fields--;
    } else { // если равно
      item.after($(newItem)); // добавляем новое поле
      item.remove(); // удаляем старое
    }
    fieldsAttached--;
    if (fields == 1) { // если поле осталось одно
      attach.find('.attach__up').text('ЗАГРУЗИТЬ СКАН ДОГОВОРА'); // меняем текст
    }
  });
});








// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });




// Календарь
$(function () {
    $('#datepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});










 // Стилизация селектов
$('select').styler();





// investments
$('.investments-item__header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.investments-item__body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.investments-item__body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.investments-item').hasClass('active'))){
    $('.investments-item').removeClass("active");
    $(this).parent('.investments-item').addClass("active");
  }else{
    $(this).parent('.investments-item').removeClass("active");
  };
});












// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");



































// MMENU
if ($(window).width() < 1200) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}







})